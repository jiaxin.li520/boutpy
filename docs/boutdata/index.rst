.. _chapter-boutdata:

========================================================
:mod:`boutpy.boutdata` -- exchanging data to/from BOUT++
========================================================

.. contents:: Table of Contents
    :local:

:class:`~boutpy.boutdata.boutgrid` -- More Information about BOUT++ Grid
------------------------------------------------------------------------

.. autoclass:: boutpy.boutdata.boutgrid
    :members:

:class:`~boutpy.boutdata.Field` -- Field Class
-----------------------------------------------------------------

.. autoclass:: boutpy.boutdata.Field
    :members:
    :undoc-members:

:mod:`~boutpy.boutdata.map_pfile2grid` -- Map Experimental Profile to Grid
--------------------------------------------------------------------------

.. autosummary::

    ~boutpy.boutdata.map_pfile2grid.map_1d4grid
    ~boutpy.boutdata.map_pfile2grid.map_nc2grid
    ~boutpy.boutdata.map_pfile2grid.map_pfile2grid

.. automodule:: boutpy.boutdata.map_pfile2grid
    :members:

:mod:`~boutpy.boutdata.collect` -- collect data
-------------------------------------------------
.. automodule:: boutpy.boutdata.collect
    :members:

:mod:`~boutpy.boutdata.restart` -- manipulate restart files
---------------------------------------------------------------
.. automodule:: boutpy.boutdata.restart
    :members:

:mod:`~boutpy.boutdata.gen_surface` -- Flux Surface Generator
----------------------------------------------------------------
.. automodule:: boutpy.boutdata.gen_surface
    :members:

:mod:`~boutpy.boutdata.pol_slice` -- Poloidal Slice
----------------------------------------------------
.. automodule:: boutpy.boutdata.pol_slice
    :members:
