.. _chapter-bin:

====================================
**bin** -- executable python scripts
====================================

.. contents:: Table of Contents
    :local:

Introduction
------------
The python scripts in this directory are executable. They are useful for
running BOUT++ code and data processing.

The following scripts only work on NERSC system:

- jobstate.py
- jobsubmit.py

Executable Scripts
------------------
The following executable scripts can be called directly in a terminal:

.. code-block:: bash

    $ jobsubmit.py -d data -e ./elm_pb -n 512 -t 10:00 -qr -r
    # -d data: path to store the BOUT.dmp.nc
    # -e ./elm_pb: executable module
    # -t 10:00: walltime limit, 10 mins
    # -qr: regular queue
    # -r: restart


.. _bin-compare_inp:

compare_inp.py -- Compare the BOUT.inp files
++++++++++++++++++++++++++++++++++++++++++++

.. program-output:: python boutpy/bin/compare_inp.py -h
    :cwd: ../../

Examples:

.. code-block:: bash

    $ compare_inp.py */n10/BOUT.inp
    cases:
                         Name
    case0  diamag/n10
    case1  diamag_er_2Er/n10
    case2  diamag_er_Er/n10
    --------------------------------------------------
    differences:
                   case0 case1 case2
    Er_factor      1.0    2.0   1.0
    diamag_er      false  true  true
    diamag_phi0    false  true  true
    experiment_Er  false  true  true


.. seealso:: :mod:`~boututils.compare_inp`

growthrate.py -- calculate growth rate
++++++++++++++++++++++++++++++++++++++

.. program-output:: python boutpy/bin/growthrate.py -h
    :cwd: ../../
    :returncode: 0

gamma_plot.py -- plot growthrate spectrum
+++++++++++++++++++++++++++++++++++++++++

.. program-output:: python boutpy/bin/gamma_plot.py -h
    :cwd: ../../

jobsubmit.py -- submit serial jobs in NERSC
+++++++++++++++++++++++++++++++++++++++++++

.. program-output:: python boutpy/bin/jobsubmit.py -h
    :cwd: ../../

jobstate.py -- check & update & delete jobs
+++++++++++++++++++++++++++++++++++++++++++

.. program-output:: python boutpy/bin/jobstate.py -h
    :cwd: ../../

scan.py -- create parameter scan
++++++++++++++++++++++++++++++++

.. program-output:: python boutpy/bin/scan.py -h
    :cwd: ../../
