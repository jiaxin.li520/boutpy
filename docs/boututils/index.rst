.. _chapter-boututils:

=======================================
:mod:`boutpy.boututils` -- useful tools
=======================================

.. contents:: Table of Contents
    :local:

:mod:`~boutpy.boututils.calculus` -- Derivatives and Integrals
--------------------------------------------------------------

.. autosummary::

    ~boutpy.boututils.calculus.deriv
    ~boutpy.boututils.calculus.integrate

.. automodule:: boutpy.boututils.calculus
    :members:

:mod:`~boutpy.boututils.compare_inp` -- parse & compare BOUT.inp
-----------------------------------------------------------------

.. autosummary::

    ~boutpy.boututils.compare_inp.parser_config
    ~boutpy.boututils.compare_inp.dict_to_level1
    ~boutpy.boututils.compare_inp.compare_inp

.. automodule:: boutpy.boututils.compare_inp
    :members:

:mod:`~boutpy.boututils.datafile` -- Netcdf File I/O
------------------------------------------------------
.. autoclass:: boutpy.boututils.datafile.DataFile

:mod:`~boutpy.boututils.fileio` -- data I/O methods
-----------------------------------------------------
.. autosummary::

    ~boutpy.boututils.fileio.save2nc
    ~boutpy.boututils.fileio.readsav
    ~boutpy.boututils.fileio.file_import
    ~boutpy.boututils.fileio.file_list

.. automodule:: boutpy.boututils.fileio
    :members:

:mod:`~boutpy.boututils.functions` -- useful functions
--------------------------------------------------------

.. autosummary::

    ~boutpy.boututils.functions.get_yesno
    ~boutpy.boututils.functions.get_nth
    ~boutpy.boututils.functions.get_limits
    ~boutpy.boututils.functions.get_input
    ~boutpy.boututils.functions.nearest
    ~boutpy.boututils.functions.pycommand
    ~boutpy.boututils.functions.nrange
    ~boutpy.boututils.functions.sort_nicely

.. automodule:: boutpy.boututils.functions
    :members:

:mod:`~boutpy.boututils.pfile` -- Osborne pfiles I/O
-----------------------------------------------------

.. automodule:: boutpy.boututils.pfile
    :members:

:mod:`~boutpy.boututils.shell` -- Run shell command
----------------------------------------------------

.. autosummary::

    ~boutpy.boututils.shell.shell
    ~boutpy.boututils.shell.jobinfo

.. automodule:: boutpy.boututils.shell
    :members:
