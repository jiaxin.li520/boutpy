.. _chapter-visualization:

==================================================
:mod:`~boutpy.visualization` -- data visualization
==================================================

.. contents:: Table of Contents
    :local:

:mod:`~boutpy.visualization.plotconfig` -- General setup for plot
-----------------------------------------------------------------

.. autosummary::
    ~boutpy.visualization.plotconfig.colors
    ~boutpy.visualization.plotconfig.colors_default
    ~boutpy.visualization.plotconfig.myplot_style
    ~boutpy.visualization.plotconfig.color_list

.. automodule:: boutpy.visualization.plotconfig
    :members:

:mod:`~boutpy.visualization.plotfigs` -- plot figures
------------------------------------------------------

.. autosummary::
    ~boutpy.visualization.plotfigs.surface
    ~boutpy.visualization.plotfigs.contourf
    ~boutpy.visualization.plotfigs.savefig

.. automodule:: boutpy.visualization.plotfigs
    :members:

