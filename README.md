
    ________ _______ _____  __________
    ___  __ )__  __ \__  / / /___  __/______ ______
    __  __  |_  / / /_  / / / __  /   ___/ /____/ /_
    _  /_/ / / /_/ / / /_/ /  _  /    /_  __//_  __/
    /_____/  \____/  \____/   /_/      /_/    /_/

BOUT++ boutpy's documentation!
=================================

**Wellcome to BOUT++ boutpy documentation**!

**BOUTPY** is a ``Python2.x`` package intended to provide some common
tools required for performing Plasmas Fluid Simulations with
 [BOUT++](http://boutproject.github.io/index.html) framework.
The **BOUTPY** in the latest release
[BOUT++ v4.0.0](https://github.com/boutproject/BOUT-dev/releases/tag/v4.0.0)
is ``Python3.x`` supported.

**Download** This repo is accessible on
[GitLab](https://gitlab.com/conderls/boutpy)

**Bugs Report** The most bugs and issues are managed using the
[issue tracker](https://gitlab.com/conderls/boutpy/issues).
All suggestions, comments and feature requests are gladly wellcome and
appreciated.

Getting Started
----------------

It's highly encouraged to use [Anaconda](https://www.continuum.io/downloads)
as the python environment.
Before starting to use the tools in this package, some envrionment variables
and required packages are suggested to be set up first.

```shell
$ pip install boutpy

# if "IOError: Permission denied" occoured, try:
$ pip install --user boutpy

```

User Manual
------------------
The document for this repo is available at
[ReadTheDocs](http://boutpy.readthedocs.io). The user manual for the latest
BOUT++ are available [here](http://bout-dev.readthedocs.io/en/latest/)
